export default [
  {
    path: '/',
    name: 'Unknown Page',
    component: () => import('@/frontend/views/home.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '*',
    redirect: {
      name: 'Unknown Page'
    }
  }
]
