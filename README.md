# default-backend

> Page for _*.flattrack.io_

## Local development

Bring up in Minikube using Tilt:
```sh
tilt up --host 0.0.0.0
```

## License
Copyright 2020 Caleb Woodbine.
This project is licensed under the [GPL-3.0](http://www.gnu.org/licenses/gpl-3.0.html) and is [Free Software](https://www.gnu.org/philosophy/free-sw.en.html).
This program comes with absolutely no warranty.
