FROM node:14.7.0-alpine3.11 AS ui
WORKDIR /app
COPY src /app/src
COPY public /app/public
COPY *.js *.json /app/
RUN npm i
RUN npm run build

ARG ARCH
FROM registry.gitlab.com/safesurfer/go-http-server:1.3.0-$ARCH
ENV APP_SERVE_FOLDER=/app/dist \
  APP_VUEJS_HISTORY_MODE=true
LABEL maintainer="Caleb Woodbine <calebwoodbine.public@gmail.com>"
COPY --from=ui /app/dist /app/dist
COPY template-map.yaml /app/map.yaml
